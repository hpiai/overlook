﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        int id;
        string nom;
        string adresse;
        string ville;

        //Constructeur

        Hotel(int unId, string unNom, string uneAdresse, string uneVille)
        {
            Id = unId;
            Nom = unNom;
            Adresse = uneAdresse;
            Ville = uneVille;
        }

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
}
